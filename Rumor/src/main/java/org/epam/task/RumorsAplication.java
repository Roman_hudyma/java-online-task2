package org.epam.task;

        import java.util.Random;
        import java.util.Scanner;

/**
 * This class we'll be using to calculate probability that everyone at  the party (except owner) will hear the rumor.
 *
 *@author Roman Hudyma
 */

public class RumorsAplication {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Random rand = new Random();
        System.out.println("Enter number of guests");
        /**
         * Number of guest at the party
         */
        double number =scanner.nextDouble();
        /**
         * Probability that everyone at  the party (except owner) will hear the rumor
         */
        double chance ;
        /**
         * Chance that person will hear the rumor
         */
        double result=1;
        int guest=0;

        for (int i =2;i<number;i++)
        {
            chance =i/number;
            result = result * chance;
        }
        for (int k=2;k<number;k++) {
            boolean random = rand.nextBoolean();
            if (random == true) {
                guest++;
            } else
            {
                k= (int) number;
            }
        }
        result =result*100;
        System.out.println("Who heard: "+guest);
        System.out.println("Probability that everyone at  the party (except Alice) will hear the rumor: "+result+"%");
    }
}